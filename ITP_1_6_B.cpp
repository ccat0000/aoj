#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    const int N=13;
    int n, mark, rank;
    char c, marks[4]={'S', 'H', 'C', 'D'};
    bool check[4][N]={{false}};

    cin >> n;
    for (int i=0; i<n; i++){
        cin >> c >> rank;
        rank--;
        for (int m=0; m<4; m++){
            if(c==marks[m]){
                mark = m;
                break;
            }
        }
        check[mark][rank] = true;
    }
    for (int m=0; m<4; m++){
        for (int r=0; r<13; r++){
            if(!(check[m][r])){
                printf("%c %d\n", marks[m], r+1);
            }
        }
    }
    return 0;
}