#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    int n, b, f, r, v;
    int resident_nums[4][3][10] = {0};
    cin >> n;
    for (int i=0; i<n; i++){
        cin >> b >> f >> r >> v;
        resident_nums[b-1][f-1][r-1] += v;
    }
    for (int i=0; i<4; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<10; k++){
                printf(" %d", resident_nums[i][j][k]);
            }
            printf("\n");
        }
        if (i < 3) printf("####################\n");
    }
    return 0;
}