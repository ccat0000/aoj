#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    string str, op, p, tmp;
    int n, a, b;
    cin >> str;
    cin >> n;
    for(int i=0; i<n; i++){
        cin >> op >> a >> b;
        if(op.compare("replace")==0){
            cin >> p;
            str.replace(a, b-a+1, p);
        }else if(op.compare("reverse")==0){
            tmp = str.substr(a, b-a+1);
            for(int j=0; j<tmp.length(); j++){
                str[a+j] = tmp[tmp.length()-1-j];
            }
        }else{
            cout << str.substr(a, b-a+1) << endl;
        }
    }
    return 0;
}