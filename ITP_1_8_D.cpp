#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    string s, p;
    bool contain;

    getline(cin, s);
    getline(cin, p);

    for(int i=0; i<s.length(); i++){
        contain = true;
        for(int j=0; j<p.length(); j++){
            if(s[(i+j)%s.length()]!=p[j]){
                contain=false;
                break;
            };
        }
        if(contain) break;
    }

    if(contain){
        printf("Yes\n");
    }else{
        printf("No\n");
    }

    return 0;
}