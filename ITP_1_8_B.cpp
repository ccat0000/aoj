#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    int sum, shift='0';
    string x;
    while(true){
        getline(cin, x);
        if(x[0]=='0') break;
        sum = 0;
        for(int i=0; i<x.length(); i++){
            sum += x[i]-shift;
        }
        printf("%d\n", sum);
    }
    return 0;
}