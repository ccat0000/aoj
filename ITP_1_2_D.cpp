#include <iostream>
#include <stdio.h>
#include <algorithm>

using namespace std;

int main(){
    int w, h, x, y, r;
    cin >> w >> h >> x >> y >> r;
    if((x-r >= 0 && x+r <= w) && (y-r >= 0 && y+r <= h)){
        printf("Yes\n");
    }else{
        printf("No\n");
    }
    return 0;
}