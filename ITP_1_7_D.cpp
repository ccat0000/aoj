#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

void get_matrix(int n, int m, int *mat){
    for (int i=0; i<n; i++){
        for (int j=0; j<m; j++){
            cin >> mat[m*i+j];
        }
    }
}
int main(){
    int n, m, l, *mat1, *mat2;
    long int *mat3;
    cin >> n >> m >> l;
    mat1 = new int[n*m];
    mat2 = new int[m*l];
    mat3 = new long int[n*l]();
    get_matrix(n, m, mat1);
    get_matrix(m, l, mat2);
    for(int i=0; i<n; i++){
        for(int j=0; j<l; j++){
            for(int k=0; k<m; k++){
                mat3[l*i+j] += mat1[m*i+k] * mat2[l*k+j];
            }
            printf("%ld", mat3[l*i+j]);
            if(j==l-1){
                printf("\n");
            }else{
                printf(" ");
            }
        }
    }
}