#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    int n, *a;
    cin >> n;
    a = new int[n];
    for(int i=0; i<n; i++){
        cin >> a[i];
    }
    for(int i=n-1; i>=0; i--){
        printf("%d", a[i]);
        if (i>0){
            printf(" ");
        }
    }
    printf("\n");
    return 0;
}