#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    int n, m, sum, *mat, *vec;
    cin >> n >> m;
    mat = new int[n*m];
    vec = new int[m];
    for(int i=0; i<n; i++){
        for (int j=0; j<m; j++){
            cin >> mat[m*i+j];
        }
    }
    for (int i=0; i<m; i++){
        cin >> vec[i];
    }
    for(int i=0; i<n; i++){
        sum = 0;
        for (int j=0; j<m; j++){
            sum += mat[m*i+j]*vec[j];
        }
        printf("%d\n", sum);
    }
    return 0;
}