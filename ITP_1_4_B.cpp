#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>

using namespace std;

int main(){
    double r, s, l;
    cin >> r;
    s = M_PI * r * r;
    l = 2 * M_PI * r;
    printf("%.6f %.6f\n", s, l);
}