#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

void tolower(string *s){
    char shift = 'a'-'A';
    for(int i=0; i<s->length(); i++){
        if((*s)[i]>='A' && (*s)[i]<='Z'){
            (*s)[i] += shift;
        }
    }
}

int main(){
    string w1, w2;
    const string end = "END_OF_TEXT";
    int cnt = 0;

    cin >> w1;
    while(true){
        cin >> w2;
        if(w2.compare(end)==0) break;
        tolower(&w2);
        if(w2.compare(w1)==0) cnt++;
    }
    printf("%d\n", cnt);
    return 0;
}