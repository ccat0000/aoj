#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    int r, c, *sheet, tmp;
    cin >> r >> c;
    sheet = new int[(r+1)*(c+1)]();
    for(int i=0; i<r; i++){
        for(int j=0; j<c; j++){
            cin >> tmp;
            sheet[(c+1)*i+j] = tmp;
            sheet[(c+1)*i+c] += tmp;
            sheet[(c+1)*(r+1)-1] += tmp;
            sheet[(c+1)*r+j] += tmp;
        }
    }
    for(int i=0; i<r+1; i++){
        for(int j=0; j<c+1; j++){
            printf("%d", sheet[(c+1)*i+j]);
            if(j==c){
                printf("\n");
            }else{
                printf(" ");
            }
        }
    }
    delete[] sheet;
}