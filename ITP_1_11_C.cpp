#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

class Dice{
    public:
        int labels[6];
        Dice(int *l);
        void permutate(int *perm);
        void rotate(char direction);
        void print_top();
        void print_all();
        bool adjust(int top, int right);
};
Dice::Dice(int *l){
    for(int i=0; i<6; i++){
        this->labels[i] = l[i];
    }
}
void Dice::permutate(int *perm){
    int tmp;
    tmp = this->labels[perm[3]];
    for(int i=2; i>=0; i--){
        this->labels[perm[i+1]] = this->labels[perm[i]];
    }
    this->labels[perm[0]] = tmp;
}

void Dice::rotate(char direction){
    int perm[4][4] = {
                      {0, 4, 5, 1}, 
                      {0, 1, 5, 4}, 
                      {0, 2, 5, 3}, 
                      {0, 3, 5, 2}}; 
    switch(direction){
        case 'N':
            permutate(perm[0]);
            break;
        case 'S':
            permutate(perm[1]);
            break;
        case 'E':
            permutate(perm[2]);
            break;
        case 'W':
            permutate(perm[3]);
            break;
    }
}

void Dice::print_top(){
    printf("%d\n", this->labels[0]);
}

void Dice::print_all(){
    for(int i=0; i<6; i++){
        printf("%3d ", this->labels[i]);
    }
    printf("\n");
}
bool Dice::adjust(int top, int front){
    const string search1="NNNNENNNN", search2="EEEE";
    bool isSame = false;

    for(int i=0; i<search1.length(); i++){
        this->rotate(search1[i]);
        if(this->labels[1]==front){
            isSame = true;
            break;
        }
    }
    if(!isSame) return false;
    for(int j=0; j<search2.length(); j++){
        this->rotate(search2[j]);
        if(this->labels[0]==top){
            isSame = true;
            break;
        }
    }
    return isSame;
}

bool isSameDice(Dice d1, Dice d2){
    if(d1.adjust(d2.labels[0], d2.labels[1])){
        for(int i=0; i<6; i++){
            if(d1.labels[i] != d2.labels[i]){
                return false;
            }
        }
        return true;
    }
    return false;
}
int main(){
    int labels1[6], labels2[6];
    for(int i=0; i<6; i++){
        cin >> labels1[i];
    }
    for(int i=0; i<6; i++){
        cin >> labels2[i];
    }
    Dice d1(labels1);
    Dice d2(labels2);
    if(isSameDice(d1, d2)){
        printf("Yes\n");
    }else{
        printf("No\n");
    }
    return 0;
}