#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>

using namespace std;

int main(){
    int n, x, min, max;
    long int sum;
    min = 1000000;
    max = -1000000;
    sum = 0;
    cin >> n;
    for(int i=0; i<n; i++){
        cin >> x;
        sum += x;
        if (x < min) min = x;
        if (x > max) max = x;
    }
    printf("%d %d %ld\n", min, max, sum);
    return 0;
}