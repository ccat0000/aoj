#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

class Dice{
    public:
        int labels[6];
        Dice(int *l);
        void permutate(int *perm);
        void rotate(char direction);
        void print_top();
        void print_all();
};
Dice::Dice(int *l){
    for(int i=0; i<6; i++){
        this->labels[i] = l[i];
    }
}
void Dice::permutate(int *perm){
    int tmp;
    tmp = this->labels[perm[3]];
    for(int i=2; i>=0; i--){
        this->labels[perm[i+1]] = this->labels[perm[i]];
    }
    this->labels[perm[0]] = tmp;
}

void Dice::rotate(char direction){
    int perm[4][4] = {
                      {0, 4, 5, 1}, 
                      {0, 1, 5, 4}, 
                      {0, 2, 5, 3}, 
                      {0, 3, 5, 2}}; 
    switch(direction){
        case 'N':
            permutate(perm[0]);
            break;
        case 'S':
            permutate(perm[1]);
            break;
        case 'E':
            permutate(perm[2]);
            break;
        case 'W':
            permutate(perm[3]);
            break;
    }
}

void Dice::print_top(){
    printf("%d\n", this->labels[0]);
}

void Dice::print_all(){
    for(int i=0; i<6; i++){
        printf("%3d ", this->labels[i]);
    }
    printf("\n");
}

int main(){
    int labels[6];
    string operations;
    for(int i=0; i<6; i++){
        cin >> labels[i];
    }
    Dice d(labels);
    cin >> operations;
    for(int i=0; i<operations.length(); i++){
        d.rotate(operations[i]);
    }
    d.print_top();
    return 0;
}