#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    int n; 
    double *x, y, sum1, sum2, sum3, m, tmp;
    cin >> n;
    x = new double[n];
    for(int i=0; i<n; i++){
        cin >> x[i];
    }
    m = 0;
    sum1 = 0;
    sum2 = 0;
    sum3 = 0;
    for(int i=0; i<n; i++){
        cin >> y;
        tmp = abs(x[i]-y);
        sum1 += pow(tmp, 1);
        sum2 += pow(tmp, 2);
        sum3 += pow(tmp, 3);
        if(m<tmp){
            m = tmp;
        }
    }
    printf("%f\n", sum1);
    printf("%f\n", sqrt(sum2));
    printf("%f\n", pow(sum3, 1.0/3.0));
    printf("%f\n", m);
    delete[] x;
    return 0;
}