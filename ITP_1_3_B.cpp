#include <iostream>
#include <stdio.h>
#include <algorithm>

using namespace std;

int main(){
    int i, x; 
    i = 1;
    while(true){
        cin >> x;
        if(x==0) break;
        printf("Case %d: %d\n", i++, x);
    }
    return 0;
}