#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>

using namespace std;

int main(){
    int a, b, c;
    char op;
    while(true){
        cin >> a >> op >> b;
        if (op == '?') break;
        switch(op){
            case '+':
                c = a + b;
                break;
            case '-':
                c = a - b;
                break;
            case '*':
                c = a * b;
                break;
            case '/':
                c = a / b;
                break;
        }
        printf("%d\n", c);
    }
    return 0;
}