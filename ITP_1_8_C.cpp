#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    int sum, shift='a'-'A', cnt[26]={0};
    string line;
    char c;
    while(getline(cin, line)){
        for(int i=0; i<line.length(); i++){
            c = line[i];
            if(c>='A' && c<='Z'){
                c += shift;
            }
            if(c>='a' & c<='z'){
                cnt[c-'a']++;
            }
        }
    }
    for(int i=0; i<26; i++){
        printf("%c : %d\n", 'a'+i, cnt[i]);
    }
    return 0;
}