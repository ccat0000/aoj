#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    double a, b, c, s, l, h;
    cin >> a >> b >> c;
    h = b * sin(c * M_PI/180);
    s = a * h / 2;
    l = sqrt(h*h + pow(a-b*cos(c*M_PI/180), 2));
    l += a + b; 
    printf("%f\n%f\n%f\n", s, l, h);
    return 0;
}