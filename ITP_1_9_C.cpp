#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    int n, score1=0, score2=0; 
    string c1, c2;
    cin >> n;
    for(int i=0; i<n; i++){
        cin >> c1 >> c2;
        if(c1.compare(c2)==0){
            score1 += 1;
            score2 += 1;
        }else if(c1.compare(c2)>0){
            score1 += 3;
        }else{
            score2 += 3;
        }
    }
    printf("%d %d\n", score1, score2);
    return 0;
}