#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    double x1, y1, x2, y2, d;
    cin >> x1 >> y1 >> x2 >> y2;
    d = sqrt(pow(x1-x2, 2) + pow(y1-y2, 2));
    printf("%.8f\n", d);
    return 0;
}