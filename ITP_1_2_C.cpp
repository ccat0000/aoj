#include <iostream>
#include <stdio.h>
#include <algorithm>

using namespace std;

int main(){
    int v[3];
    cin >> v[0] >> v[1] >> v[2];
    sort(v, v+3);
    printf("%d %d %d\n", v[0], v[1], v[2]);
    return 0;
}