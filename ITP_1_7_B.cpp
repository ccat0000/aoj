#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    int n, x, cnt;
    while (true){
        cin >> n >> x;
        if(n==0 && x==0) break;
        cnt = 0;
        for(int i=1; i<=n-2; i++){
            for(int j=i+1; j<=n-1; j++){
                for(int k=j+1; k<=n; k++){
                    if(i+j+k == x) cnt++;
                }
            }
        }
        printf("%d\n", cnt);
    }
    return 0;
}