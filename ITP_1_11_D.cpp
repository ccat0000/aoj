#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

class Dice{
    public:
        int labels[6];
        Dice(int *l);
        void set_labels(int *l);
        void permutate(int *perm);
        void rotate(char direction);
        void print_top();
        void print_all();
        bool adjust(int top, int right);
};
Dice::Dice(int *l){
    this->set_labels(l);
}
void Dice::set_labels(int *l){
    for(int i=0; i<6; i++){
        this->labels[i] = l[i];
    }
}
void Dice::permutate(int *perm){
    int tmp;
    tmp = this->labels[perm[3]];
    for(int i=2; i>=0; i--){
        this->labels[perm[i+1]] = this->labels[perm[i]];
    }
    this->labels[perm[0]] = tmp;
}

void Dice::rotate(char direction){
    int perm[4][4] = {
                      {0, 4, 5, 1}, 
                      {0, 1, 5, 4}, 
                      {0, 2, 5, 3}, 
                      {0, 3, 5, 2}}; 
    switch(direction){
        case 'N':
            permutate(perm[0]);
            break;
        case 'S':
            permutate(perm[1]);
            break;
        case 'E':
            permutate(perm[2]);
            break;
        case 'W':
            permutate(perm[3]);
            break;
    }
}

void Dice::print_top(){
    printf("%d\n", this->labels[0]);
}

void Dice::print_all(){
    for(int i=0; i<6; i++){
        printf("%3d ", this->labels[i]);
    }
    printf("\n");
}
bool Dice::adjust(int top, int front){
    const string search1="NNNNENNNN", search2="EEEE";
    bool isSame = false;

    for(int i=0; i<search1.length(); i++){
        this->rotate(search1[i]);
        if(this->labels[1]==front){
            isSame = true;
            break;
        }
    }
    if(!isSame) return false;
    for(int j=0; j<search2.length(); j++){
        this->rotate(search2[j]);
        if(this->labels[0]==top){
            isSame = true;
            break;
        }
    }
    return isSame;
}

bool isSameDice(Dice d1, Dice d2){
    string patterns[6] = {"", "S", "S", "S", "ES", "SS"}; // 2, 1, 5, 6, 4, 3
    bool isSame;
    for(int i=0; i<6; i++){
        for(int j=0; j<patterns[i].length(); j++){
            d1.rotate(patterns[i][j]);
        }
        for(int j=0; j<4; j++){
            d1.rotate('E');
            isSame = true;
            for(int k=0; k<6; k++){
                if(d1.labels[k]!=d2.labels[k]){
                    isSame = false;
                    break;
                }
            }
            if(isSame) return true;
        }
    }
    return false;
}

int main(){
    int labels[6], n;
    bool containSame = false;
    Dice **d;
    cin >> n;
    d = new Dice*[n];
    for(int i=0; i<n; i++){
        for(int j=0; j<6; j++){
            cin >> labels[j];
        }
        d[i] = new Dice(labels);
    }
    for(int i=0; i<n-1; i++){
        for(int j=i+1; j<n; j++){
            if(isSameDice(*d[i], *d[j])){
                containSame = true;
                break;
            }
        }
        if(containSame) break;
    }
    if(containSame){
        printf("No\n");
    }else{
        printf("Yes\n");
    }
    for(int i=0; i<n; i++) delete d[i];
    delete[] d;
    return 0;
}