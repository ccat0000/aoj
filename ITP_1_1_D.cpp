#include <iostream>
#include <stdio.h>

using namespace std;

int main(){
    int t, s, m;
    cin >> t;
    s = t % 60;
    t /= 60;
    m = t % 60;
    t /= 60;
    printf("%d:%d:%d\n", t, m, s);
    return 0;
}