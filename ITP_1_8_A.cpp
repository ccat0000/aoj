#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    string line;
    int shift='a'-'A';
    char c;

    getline(cin, line); 
    for(int i=0; i<line.length(); i++){
        c = line[i];
        if(c>='A' && c<='Z'){
            printf("%c", c+shift);
        }else if(c>='a' && c<= 'z'){
            printf("%c", c-shift);
        }else{
            printf("%c", c);
        }
    }
    printf("\n");
    return 0;
}