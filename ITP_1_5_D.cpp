#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>

using namespace std;

int main(){
    int n, x;
    cin >> n;
    for(int i=1; i<=n; i++){
        for(int x=i; x>0; x/=10){
            if (i % 3 == 0 || x % 10 == 3){
                printf(" %d", i);
                break;
            }
        }
    }
    printf("\n");
    return 0;
}