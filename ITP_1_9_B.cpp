#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    string deck;
    int m, h, sum;
    while(true){
        cin >> deck;
        if(deck.compare("-")==0) break;
        cin >> m;
        sum = 0;
        for(int i=0; i<m; i++){
            cin >> h;
            sum += h;
        }
        sum %= deck.length();
        for(int i=sum; i<deck.length(); i++){
            printf("%c", deck[i]);
        }
        for(int i=0; i<sum; i++){
            printf("%c", deck[i]);
        }
        printf("\n");
    }
    return 0;
}