#define _USE_MATH_DEFINES
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

int main(){
    int n; 
    double sum, s, v;
    while(true){
        cin >> n;
        if(n==0) break;

        sum = 0;
        v = 0;
        for(int i=0; i<n; i++){
            cin >> s;
            sum += s;
            v += s*s;
        }
        v = v/n - (sum/n)*(sum/n);
        printf("%f\n", sqrt(v));
    }
    return 0;
}